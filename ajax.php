<?php
include_once('db_access.php');
//http_response_code(200);

$id = isset($_POST["id"]) ? $_POST["id"] : "";
$date = isset($_POST["date"]) ? $_POST["date"] : "";

//データをINSERTする
$rows = execute_sql(
    'SELECT count(*) AS `cnt` FROM `dates` WHERE `id` = ?;',
    array($id)
);
if($rows[0]['cnt'] == 0){
    $ret = execute_sql(
    'INSERT INTO `dates` (`id`,`date`,`deleted`) VALUES(?,?,?);',
    array($id,$date,false)
);
}else {
    $ret = execute_sql(
        'UPDATE `dates` SET `date` = ? WHERE id = ?',
        array($date,$id)
    );
}
$result = array(
    'status' => (int)true,
  //  'id' => $id,
//    'date' => $date
);

echo json_encode($result);